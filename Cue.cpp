#include "Cue.h"
#include "stb_image.h"
#define STB_IMAGE_IMPLEMENTATION
#pragma comment(lib, "glew32.lib")

Cue::Cue(int id, int radius, int height, btVector3& origin, int massa, float friction, float restitution,
		string texture, btDiscreteDynamicsWorld* world)
{
	this->iD = id;
	bulletObject = new BulletObject(new btCapsuleShape(radius, height), origin, massa, btVector3(0, 0, 0));
	bulletObject->setFriction(friction);
	bulletObject->setRestitution(restitution);

	cueStick = CaveLib::loadModel("data/Pool3D/Cue/untitled.obj");
	if (!texture.empty())
		this->texture = CaveLib::loadTexture(texture);
	else
		textureEmpty = true;
	//world->addRigidBody(bulletObject->body);
}


Cue::~Cue()
{
}

void Cue::draw()
{
	bulletObject->body->activate(true);
	glEnable(GL_TEXTURE_2D);
	bulletObject->draw();
	if (!textureEmpty)
		glBindTexture(GL_TEXTURE_2D, texture->tid());
	else
		glDisable(GL_TEXTURE_2D);
	cueStick->draw(NULL);
}
