#pragma once
#include <btBulletDynamicsCommon.h>
#include <GL/glew.h>

class BulletObject
{
public:
	BulletObject(btCollisionShape* const &shape, btVector3& origin, float mass, btVector3& fallInertia);
	~BulletObject();

	void setFriction(float);
	void setRestitution(float);
	void draw();
	void setOrigin(int x, int y, int z);

	btRigidBody* body;
};

