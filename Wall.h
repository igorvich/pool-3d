#pragma once
#include "BulletObject.h"

class Wall
{
private:
	BulletObject* muur;
public:
	Wall(btCollisionShape* wallShape, btVector3& origin, btDiscreteDynamicsWorld* world, bool holeWall);
	~Wall();

	void draw();
};

