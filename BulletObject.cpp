#include "BulletObject.h"
btTransform floorTransform;
BulletObject::BulletObject(btCollisionShape* const &shape, btVector3& origin, float mass, btVector3& fallInertia)
{
	//Bullet initialisatie
	floorTransform.setIdentity();
	floorTransform.setOrigin(origin);

	btDefaultMotionState* groundMationState = new btDefaultMotionState(floorTransform);
	btRigidBody::btRigidBodyConstructionInfo bodyCI(mass, groundMationState, shape, fallInertia);
	body = new btRigidBody(bodyCI);
}

BulletObject::~BulletObject()
{
}

void BulletObject::setOrigin(int x, int y, int z)
{
	floorTransform.setOrigin(btVector3(x,y,z));
}

void BulletObject::setFriction(float friction)
{
	//Wrijving
	body->setFriction(friction);
}

void BulletObject::setRestitution(float restitution)
{
	//Wordt gebruikt voor bouncing
	body->setRestitution(restitution);
}

void BulletObject::draw()
{
	//Ophalen van matrix voor tekenen object
	btTransform trans;
	btScalar m[16];
	
	body->getMotionState()->getWorldTransform(trans);
	trans.getOpenGLMatrix(m);
	glMultMatrixf(m);
}
