#pragma once
#include <GL/glew.h>
#include <iostream>
class FloorPane
{
public:
	FloorPane(float x, float y, float z, float size);
	FloorPane();
	~FloorPane();
	void display();
private:
	float X, Y, Z, Size;
};

