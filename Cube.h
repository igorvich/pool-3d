#pragma once
#include <GL/glew.h>
#include <iostream>

class Cube{
public:
	Cube(float x, float y, float z, float alpha, float size, int rot, int rotX, int rotY, int rotZ);
	Cube();
	~Cube();

	float getPosX()  { return x; }
	float getPosY()  { return y; }
	float getPosZ()  { return z; }
	float getAlpha() { return alpha; }
	float getSize()  { return size; }
	int getRotation() { return rotation; }
	int getRotationX() { return rotationX; }
	int getRotationY() { return rotationY; }
	int getRotationZ() { return rotationZ; }

	void setPosX(float);
	void setPosY(float);
	void setPosZ(float);
	void setAlpha(float);
	void setSize(float);
	void setColor(float, float, float);
	void setRotation(int);
	void setRotationX(int);
	void setRotationY(int);
	void setRotationZ(int);

	void setVisible(bool onderkant, bool bovenkant, bool voorkant, bool achterkant, bool rechterkant, bool linkerkant);

	void display();

private:
	float x, y, z, alpha;
	float r, g, b;
	float size;
	int rotation, rotationX, rotationY, rotationZ;
	bool ondervlak, bovenvlak, voorvlak, achtervlak, rechtervlak, linkervlak;
};

