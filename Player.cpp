#include "Player.h"


Player::Player()
{
}


Player::~Player()
{
}

void Player::setShooting(bool shoot)
{
	this->shooting = shoot;
}

bool Player::isShooting()
{
	return this->shooting;
}

void Player::incrementScore()
{
	this->score++;
}

void Player::misses(bool missed)
{
	this->miss = missed;
}

bool Player::hasMissed()
{
	return miss;
}

void Player::setId(int Id)
{
	this->ID = Id;
}

int Player::getId()
{
	return this->ID;
}

void Player::setBallType(int type)
{
	this->ballType = type;
}