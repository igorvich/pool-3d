#include "FloorPane.h"


FloorPane::FloorPane(float x, float y, float z, float size)
{
	X = x;
	Y = y;
	Z = z;
	Size = size;
}


FloorPane::~FloorPane()
{
}

void FloorPane::display()
{
	glBegin(GL_QUADS);

	glNormal3f(0, -1, 0);
	glTexCoord2f(0, 0);			glVertex3f(-Size / 2 + X, -Size / 2 + Y, Size / 2 + Z);
	glTexCoord2f(0, 1);			glVertex3f(Size / 2 + X, -Size / 2 + Y, Size / 2 + Z);
	glTexCoord2f(1, 1);			glVertex3f(Size / 2 + X, -Size / 2 + Y, -Size / 2 + Z);
	glTexCoord2f(1, 0);			glVertex3f(-Size / 2 + X, -Size / 2 + Y, -Size / 2 + Z);

	glEnd();
}