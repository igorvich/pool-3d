#include <cstdlib>
#include "MyApplication.h"
#include <VrLib/Kernel.h>

int main(int argc, char* argv[])
{
	Kernel* kernel = Kernel::getInstance();
	MyApplication* application = new MyApplication();

	for (int i = 1; i < argc; i++)
	{
		if (strcmp(argv[i], "--config") == 0)
		{
			i++;
			kernel->loadConfig(argv[i]);
		}
	}

	kernel->setApp(application);
	kernel->start();
	return 0;
}