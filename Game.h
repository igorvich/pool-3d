#pragma once
#include "Player.h"
class Game
{
private:
	int nrOfPlayers;
	Player* player1;
	Player* player2;
public:
	Game();
	Game(int players);
	~Game();
	void changePlayerToShoot();
	void startGame();
	void setScoreToPlayer(Player* play);
	Player* getPlayerShooting();
	Player* getPlayerNotShooting();
	void setBallType(Player* play, int type);
	int getBallType(Player* play);
};

