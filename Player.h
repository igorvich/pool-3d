#pragma once
class Player
{
private:
	bool shooting = false;
	int score = 0;
	int ID;
	bool miss = false;
	int ballType = 0;
public:
	Player();
	~Player();
	void incrementScore();
	int getScore(){ return score; }
	bool isShooting();
	void setShooting(bool shoot);
	void setId(int ID);
	int getId();
	bool hasMissed();
	void misses(bool miss);
	int getBallType(){ return this->ballType; }
	void setBallType(int type);
};

