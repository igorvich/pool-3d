#pragma once
#include <VrLib/Application.h>
#include <VrLib/Device.h>
#include <GL/glew.h>

#ifdef _DEBUG
#pragma comment(lib, "Bullet3Common_vs2010_debug.lib")
#pragma comment(lib, "Bullet3Collision_vs2010_debug.lib")
#pragma comment(lib, "Bullet3Dynamics_vs2010_debug.lib")
#pragma comment(lib, "BulletCollision_vs2010_debug.lib")
#pragma comment(lib, "BulletDynamics_vs2010_debug.lib")
#pragma comment(lib, "Bullet3Geometry_vs2010_debug.lib")
#pragma comment(lib, "LinearMath_vs2010_debug.lib")
#else
#pragma comment(lib, "Bullet3Common_vs2010.lib")
#pragma comment(lib, "Bullet3Collision_vs2010.lib")
#pragma comment(lib, "Bullet3Dynamics_vs2010.lib")
#pragma comment(lib, "BulletCollision_vs2010.lib")
#pragma comment(lib, "BulletDynamics_vs2010.lib")
#pragma comment(lib, "Bullet3Geometry_vs2010.lib")
#pragma comment(lib, "LinearMath_vs2010.lib")
#endif

#include <btBulletDynamicsCommon.h>
#include <cavelib/CaveLib.h>
#include <cavelib/texture.h>
#include <CaveLib/model.h>
#include <CaveLib\Shader.h>
#include <string>
#include <vector>
using namespace std;

#include "Cube.h"
#include "BulletObject.h"
#include "BulletDebugDraw.h"
#include "PoolBal.h"
#include "Cue.h"
#include "Wall.h"
#include "FloorPane.h"

#include <glm/gtc/matrix_transform.hpp>

class MyApplication : public Application
{
	PositionalDevice head;

	//Bullet
	btBroadphaseInterface*                  broadphase;
	btDefaultCollisionConfiguration*        collisionConfiguration;
	btCollisionDispatcher*                  dispatcher;
	btSequentialImpulseConstraintSolver*    solver;
	btDiscreteDynamicsWorld*                world;

	BulletDebugDraw* debugdraw;

	bool debug = false;
	bool fall = false;

	//Objecten
	cModel* BiljardTable;
	cModel* cuePool;
	cTexture* skyboxTexture;
	cTexture* groundtexture;
	cTexture* muurtexture;
	cTexture* balltexture;
	BulletObject* BiljardTableObject;
	BulletObject* vloer;
	BulletObject* invisVloer;
	vector<Wall*> muren;
	
	PoolBal* whiteBal;
	vector<PoolBal*> balls;
	Cue* cueStick;
	cModel* ball;
	BulletObject* biljartBall;


public:
	MyApplication();
	~MyApplication();

	virtual void init();
	virtual void preFrame(double frameTime, double totalTime);
	virtual void draw(const glm::mat4 &projectionMatrix, const glm::mat4 &modelviewMatrix);

	void makeBulletObjects();
	void checkButtons();
	void checkBallsPocketed();
	void placeCueToWhiteBall();
	void createPoolBalls();
};

