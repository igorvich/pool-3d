#include "Wall.h"


Wall::Wall(btCollisionShape* wallShape, btVector3& origin, btDiscreteDynamicsWorld* world, bool holeWall)
{
	muur = new BulletObject(wallShape, origin, 0, btVector3(0, 0, 0));
	if (!holeWall)
		muur->setRestitution(0.5f);
	world->addRigidBody(muur->body);
	
}


Wall::~Wall()
{
}

void Wall::draw()
{
	muur->draw();
}