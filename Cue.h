#pragma once
#include "BulletObject.h"
#include <CaveLib/model.h>
#include <cavelib/texture.h>
#include <string>

using namespace std;

class Cue
{
	int iD;
	BulletObject* bulletObject;
	cModel* cueStick;
	cTexture* texture;
	bool textureEmpty = false;
	GLuint textureId;
	//Shader* shader;
public:
	Cue(int iD, int radius, int height, btVector3& origin, int massa, float friction,
		float restitution, string texture, btDiscreteDynamicsWorld* world);
	~Cue();
	void draw();
	int getId(){ return iD; }
	btRigidBody* getBulletBody(){ return bulletObject->body; }
	Cue* getCue(){ return this; }
};

