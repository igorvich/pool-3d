#include "Cube.h"

Cube::Cube(float x, float y, float z, float angle, float size, int rot, int rotX, int rotY, int rotZ) :
x(x), y(y), z(z), alpha(angle), size(size), rotation(rot), rotationX(rotX), rotationY(rotY), rotationZ(rotZ) {
	this->r = 1.0f;
	this->g = 1.0f;
	this->b = 1.0f;
}
Cube::~Cube() {
	//std::cout << "Removed cube" << std::endl;
}

void Cube::setPosX(float x) {
	this->x = x;
}
void Cube::setPosY(float y) {
	this->y = y;
}
void Cube::setPosZ(float z) {
	this->z = z;
}

void Cube::setRotationX(int rotX) {
	this->rotationX = rotX;
}

void Cube::setRotationY(int rotY) {
	this->rotationY = rotY;
}

void Cube::setRotationZ(int rotZ) {
	this->rotationZ = rotZ;
}

void Cube::setAlpha(float alp) {
	this->alpha = alp;
	if (alpha > 360)
		alpha = 0;
}
void Cube::setSize(float siz) {
	this->size = siz;
}

void Cube::setColor(float red, float green, float blue) {
	this->r = red;
	this->g = green;
	this->b = blue;
}

void Cube::setVisible(bool ondervlak, bool bovenvlak, bool voorvlak, bool achtervlak, bool rechtervlak, bool linkervlak)
{
	this->ondervlak = ondervlak;
	this->bovenvlak = bovenvlak;
	this->voorvlak = voorvlak;
	this->achtervlak = achtervlak;
	this->rechtervlak = rechtervlak;
	this->linkervlak = linkervlak;
}


void Cube::display() {

	
	glBegin(GL_QUADS);

	//ondervlak
	if (ondervlak)
	{
		glNormal3f(0, -1, 0);
		glTexCoord2f(0, 0);					glVertex3f(-size / 2 + x, -size / 2 + y, size / 2 + z);
		glTexCoord2f(0, 1);			glVertex3f(size / 2 + x, -size / 2 + y, size / 2 + z);
		glTexCoord2f(1, 1);			glVertex3f(size / 2 + x, -size / 2 + y, -size / 2 + z);
		glTexCoord2f(1, 0);					glVertex3f(-size / 2 + x, -size / 2 + y, -size / 2 + z);
	}

	//achtervlak
	if (achtervlak)
	{
		glNormal3f(0, 0, 1);
		glTexCoord2f(0, 0);					glVertex3f(size / 2 + x, size / 2 + y, -size / 2 + z);
		glTexCoord2f(0, 1);			glVertex3f(size / 2 + x, -size / 2 + y, -size / 2 + z);
		glTexCoord2f(1, 1);			glVertex3f(-size / 2 + x, -size / 2 + y, -size / 2 + z);
		glTexCoord2f(1, 0);					glVertex3f(-size / 2 + x, size / 2 + y, -size / 2 + z);
	}

	//rechtervlak
	if (rechtervlak)
	{
		glNormal3f(1, 0, 0);
		glTexCoord2f(0, 0);					glVertex3f(size / 2 + x, size / 2 + y, size / 2 + z);
		glTexCoord2f(0, 1);			glVertex3f(size / 2 + x, -size / 2 + y, size / 2 + z);
		glTexCoord2f(1, 1);			glVertex3f(size / 2 + x, -size / 2 + y, -size / 2 + z);
		glTexCoord2f(1, 0);					glVertex3f(size / 2 + x, size / 2 + y, -size / 2 + z);
	}

	//linkervlak
	if (linkervlak)
	{
		glNormal3f(-1, 0, 0);
		glTexCoord2f(0, 0);					glVertex3f(-size / 2 + x, size / 2 + y, -size / 2 + z);
		glTexCoord2f(0, 1);			glVertex3f(-size / 2 + x, -size / 2 + y, -size / 2 + z);
		glTexCoord2f(1, 1);			glVertex3f(-size / 2 + x, -size / 2 + y, size / 2 + z);
		glTexCoord2f(1, 0);					glVertex3f(-size / 2 + x, size / 2 + y, size / 2 + z);
	}

	//voorvlak
	if (voorvlak)
	{
		glNormal3f(0, 0, -1);
		glTexCoord2f(0, 0);			glVertex3f(-size / 2 + x, size / 2 + y, size / 2 + z);
		glTexCoord2f(0, 1);	glVertex3f(-size / 2 + x, -size / 2 + y, size / 2 + z);
		glTexCoord2f(1, 1);	glVertex3f(size / 2 + x, -size / 2 + y, size / 2 + z);
		glTexCoord2f(1, 0);			glVertex3f(size / 2 + x, size / 2 + y, size / 2 + z);
	}

	//bovenvlak
	if (bovenvlak)
	{
		glNormal3f(0, 1, 0);
		glTexCoord2f(0, 0);					glVertex3f(-size / 2 + x, size / 2 + y, size / 2 + z);
		glTexCoord2f(0, 1);			glVertex3f(size / 2 + x, size / 2 + y, size / 2 + z);
		glTexCoord2f(1, 1); glVertex3f(size / 2 + x, size / 2 + y, -size / 2 + z);
		glTexCoord2f(1, 0);			glVertex3f(-size / 2 + x, size / 2 + y, -size / 2 + z);
	}

	glEnd();
}

