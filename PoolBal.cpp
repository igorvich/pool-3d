#include "PoolBal.h"
#include "stb_image.h"
#define STB_IMAGE_IMPLEMENTATION
#pragma comment(lib, "glew32.lib")

PoolBal::PoolBal(int id, float radius, btVector3& origin, int massa,
				float friction, float restitution, string texture,
				btDiscreteDynamicsWorld* world)
{
	this->id = id;
	btSphereShape* ballShape = new btSphereShape(radius);
	btVector3 inertia;
	ballShape->calculateLocalInertia(massa, inertia);

	bulletObject = new BulletObject(ballShape, origin, massa, inertia);
	bulletObject->setFriction(friction);
	bulletObject->setRestitution(restitution);

	ball = CaveLib::loadModel("sphere.shape");
	if (!texture.empty())
		this->texture = CaveLib::loadTexture(texture);
	else
		textureEmpty = true;
	world->addRigidBody(bulletObject->body);

	location[0] = origin[0];
	location[1] = origin[1];
	location[2] = origin[2];

	textureShader = new ShaderProgram("data/pool3d/shaders/texture.vs", "data/pool3d/shaders/texture.fs");
	textureShader->link();
}


PoolBal::~PoolBal()
{
}

void PoolBal::setLocation(int x, int y, int z)
{
	bulletObject->setOrigin(x, y, z);
}

void PoolBal::draw(glm::mat4 mvp, glm::mat4 modelViewMatrix)
{
	glPushMatrix();
	bulletObject->body->activate(true);


	glEnable(GL_TEXTURE_2D);
	bulletObject->draw();
	btVector3 vector = bulletObject->body->getCenterOfMassPosition();
	mvp = glm::translate(mvp, glm::vec3(vector.x(),vector.y(),vector.z()));
	
	if (!textureEmpty)
		glBindTexture(GL_TEXTURE_2D, texture->tid());
	else
		glDisable(GL_TEXTURE_2D);

	//textureShader->use();
	//textureShader->getUniformLocation("mvp");
	//textureShader->setUniformMatrix4("mvp", mvp);
	//program->setUniformMatrix3("normalMatrix", normalMat);
	//textureShader->setUniformVec3("normalMatrix", modelViewMatrix);
	//ball->draw(textureShader);
	ball->draw();
	glPopMatrix();

	glUseProgram(NULL);

	/*glm::vec2 longitudeLatitude1 = glm::vec2((-atan2(y1, x1) / 3.1415926 + 1.0) * 0.5, (asinf(z1) / 3.1415926 + 0.5));
	glm::vec2 longitudeLatitude2 = glm::vec2((-atan2(y2, x2) / 3.1415926 + 1.0) * 0.5, (asinf(z2) / 3.1415926 + 0.5));
	glm::vec2 longitudeLatitude3 = glm::vec2((-atan2(y3, x3) / 3.1415926 + 1.0) * 0.5, (asinf(z3) / 3.1415926 + 0.5));
	glm::vec2 longitudeLatitude4 = glm::vec2((-atan2(y4, x4) / 3.1415926 + 1.0) * 0.5, (asinf(z4) / 3.1415926 + 0.5));

	vertices.push_back(VertexPositionNormalTexture(glm::vec3(x1, y1, z1), glm::vec3(x1, y1, z1), longitudeLatitude1));
	vertices.push_back(VertexPositionNormalTexture(glm::vec3(x2, y2, z2), glm::vec3(x2, y2, z2), longitudeLatitude2));
	vertices.push_back(VertexPositionNormalTexture(glm::vec3(x3, y3, z3), glm::vec3(x3, y3, z3), longitudeLatitude3));

	vertices.push_back(VertexPositionNormalTexture(glm::vec3(x1, y1, z1), glm::vec3(x1, y1, z1), longitudeLatitude1));
	vertices.push_back(VertexPositionNormalTexture(glm::vec3(x2, y2, z2), glm::vec3(x2, y2, z2), longitudeLatitude2));
	vertices.push_back(VertexPositionNormalTexture(glm::vec3(x4, y4, z4), glm::vec3(x4, y4, z4), longitudeLatitude4));*/
}
