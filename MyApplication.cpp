#include "MyApplication.h"
#include "Game.h"
#include <stdlib.h>
#include <sstream>
#include "ballId.h"


Game game;
vector<BulletObject*> billiardballs;
vector<ballId*> ballIds;
vector<FloorPane> floorCubes;
Cube* skybox;
int iD = 1;
string text = "data/Pool3D/PoolBalls/", extension = ".jpg";
std::ostringstream beginTexture, texture;
bool oculus = false;

MyApplication::MyApplication()
{
	game = Game(2);
}


MyApplication::~MyApplication()
{
}

void MyApplication::init()
{
	head.init("MainUserHead");
	debugdraw = new BulletDebugDraw();

	//bullet
	broadphase = new btDbvtBroadphase();
	collisionConfiguration = new btDefaultCollisionConfiguration();
	dispatcher = new btCollisionDispatcher(collisionConfiguration);
	solver = new btSequentialImpulseConstraintSolver();
	world = new btDiscreteDynamicsWorld(dispatcher, broadphase, solver, collisionConfiguration);

	world->setGravity(btVector3(0, -9.8f, 0));
	world->setDebugDrawer(debugdraw);

	makeBulletObjects();

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
}

void MyApplication::preFrame(double frameTime, double totalTime)
{
	checkButtons();
	checkBallsPocketed();
	placeCueToWhiteBall();
}

void MyApplication::draw(const glm::mat4 &projectionMatrix, const glm::mat4 &modelviewMatrix)
{
	glDisable(GL_CULL_FACE);
	glClear(GL_DEPTH_BUFFER_BIT);

	glm::mat4 mvp = projectionMatrix * modelviewMatrix;
	/*glm::mat4 normalMatrix = glm::mat4(glm::transpose(glm::inverse(modelviewMatrix)));
	glm::vec4 normalVec = normalMatrix * glm::vec4(0, 0, 0, 0);
	glm::vec3 normalVec3;
	normalVec3[0] = normalVec[0];
	normalVec3[1] = normalVec[1];
	normalVec3[2] = normalVec[2];*/

	//bullet sim
		world->stepSimulation(1 / 60.f, 10);
	if (debug)
		world->debugDrawWorld();

	glPushMatrix();
	glTranslatef(0.5f, 0.0f, -35.5f);

	//PoolTable
	glRotatef(90, 0, 1, 0);
	glTranslatef(0, -32.2, -2);
	BiljardTable->draw();
	glPopMatrix();
	glColor3f(1.0f, 1.0f, 1.0f);

	glEnable(GL_TEXTURE_2D);

	//Skybox
	glBindTexture(GL_TEXTURE_2D, skyboxTexture->tid());
	skybox->display();

	glBindTexture(GL_TEXTURE_2D, groundtexture->tid());
	for (int i = 0; i < floorCubes.size(); i++)
	{
		floorCubes[i].display();
	}

	//Vloer
	/*vloer->draw();
	glBindTexture(GL_TEXTURE_2D, groundtexture->tid());
	Cube grond(0, 15, 0, 0, 30, 0, 0, 0, 0);
	grond.setVisible(true, false, false, false, false, false);
	grond.display();*/

	//Ballen
	whiteBal->draw(mvp, modelviewMatrix);
	for (int i = 0; i < balls.size(); i++)
	{
		balls[i]->draw(mvp, modelviewMatrix);
	}

	//cueStick->draw();
}

void MyApplication::makeBulletObjects()
{
	//bullet aanmaken object
	btCollisionShape* invisgroundShape = new btStaticPlaneShape(btVector3(0, 1, 0), 1);
	btCollisionShape* groundShape = new btBoxShape(btVector3(31.4, 0, 15.5));
	btCollisionShape* wallAShape = new btBoxShape(btVector3(13.8, 4, 1));
	btCollisionShape* wallBShape = new btBoxShape(btVector3(1, 4, 14));
	btCollisionShape* ballShape = new btSphereShape(1.0f);

	btCollisionShape* holeWallAShape = new btBoxShape(btVector3(1, 4, 3));
	btCollisionShape* holeWallBShape = new btBoxShape(btVector3(3, 4, 1));
	btCollisionShape* holeWallCShape = new btBoxShape(btVector3(1, 4, 4));

	vloer = new BulletObject(groundShape, btVector3(0, -9, -36), 0, btVector3(0, 0, 0));
	vloer->setRestitution(0.5f);
	vloer->setFriction(1.0f);
	world->addRigidBody(vloer->body);

	invisVloer = new BulletObject(invisgroundShape, btVector3(0, -14, 0), 0, btVector3(0, 0, 0));
	world->addRigidBody(invisVloer->body);

	//achtermuren
	Wall* muur = new Wall(wallAShape, btVector3(-16, -5, -53.4), world, false);
	muren.push_back(muur);

	muur = new Wall(wallAShape, btVector3(16, -5, -53.4), world, false);
	muren.push_back(muur);

	//voormuren
	muur = new Wall(wallAShape, btVector3(-16, -5, -18.5), world, false);
	muren.push_back(muur);

	muur = new Wall(wallAShape, btVector3(16, -5, -18.5), world, false);
	muren.push_back(muur);

	//rechtermuur
	muur = new Wall(wallBShape, btVector3(33.3, -5, -36), world, false);
	muren.push_back(muur);

	//linkermuur
	muur = new Wall(wallBShape, btVector3(-33.3, -5, -36), world, false);
	muren.push_back(muur);

	cueStick = new Cue(1, 0.5, 12, btVector3(10, -4, -36),
		50, 0.5, 1.0, "", world);

	//Hole muren
	//Hole achter
	muur = new Wall(holeWallAShape, btVector3(-3.2, -5, -55.4), world, true);
	muren.push_back(muur);

	muur = new Wall(holeWallAShape, btVector3(3.2, -5, -55.4), world, true);
	muren.push_back(muur);

	muur = new Wall(holeWallBShape, btVector3(0, -5, -57), world, true);
	muren.push_back(muur);

	//Hole links achter
	muur = new Wall(holeWallAShape, btVector3(-28.8, -5, -55.4), world, true);
	muren.push_back(muur);

	muur = new Wall(holeWallCShape, btVector3(-35.8, -5, -53.4), world, true);
	muren.push_back(muur);

	muur = new Wall(holeWallBShape, btVector3(-32.8, -5, -57.4), world, true);
	muren.push_back(muur);

	//Hole rechts achter
	muur = new Wall(holeWallAShape, btVector3(28.8, -5, -55.4), world, true);
	muren.push_back(muur);

	muur = new Wall(holeWallCShape, btVector3(35.8, -5, -53.4), world, true);
	muren.push_back(muur);

	muur = new Wall(holeWallBShape, btVector3(32.8, -5, -57.4), world, true);
	muren.push_back(muur);

	//Hole voor
	muur = new Wall(holeWallAShape, btVector3(-3.2, -5, -16.5), world, true);
	muren.push_back(muur);

	muur = new Wall(holeWallAShape, btVector3(3.2, -5, -16.5), world, true);
	muren.push_back(muur);

	muur = new Wall(holeWallBShape, btVector3(0, -5, -14.9), world, true);
	muren.push_back(muur);

	//Hole links voor
	muur = new Wall(holeWallAShape, btVector3(-28.8, -5, -16.5), world, true);
	muren.push_back(muur);

	muur = new Wall(holeWallCShape, btVector3(-35.8, -5, -18.5), world, true);
	muren.push_back(muur);

	muur = new Wall(holeWallBShape, btVector3(-32.8, -5, -14.9), world, true);
	muren.push_back(muur);

	//Hole rechts voor
	muur = new Wall(holeWallAShape, btVector3(28.8, -5, -16.5), world, true);
	muren.push_back(muur);

	muur = new Wall(holeWallCShape, btVector3(35.8, -5, -18.5), world, true);
	muren.push_back(muur);

	muur = new Wall(holeWallBShape, btVector3(32.8, -5, -14.9), world, true);
	muren.push_back(muur);

	//Ballen
	float friction = 1.5, restitution = 1.0;
	int mass = 20;
	whiteBal = new PoolBal(0, 1, btVector3(20, -8, -36), mass, friction, restitution, "data/Pool3D/PoolBalls/BallCue.jpg", world);

	for (int z = -40; z < -30; z+=2)
	{
		int x = -24;
		beginTexture << text << iD;
		beginTexture << extension;
		balls.push_back(new PoolBal(iD, 1, btVector3(x, -8, z), mass, friction, restitution, beginTexture.str(), world));
		iD++;
		cout << beginTexture.str() << endl;
		beginTexture.str("");
		beginTexture.clear();
	}
	for (int z = -39; z < -31; z+=2)
	{
		int x = -22;
		beginTexture << text << iD;
		beginTexture << extension;
		balls.push_back(new PoolBal(iD, 1, btVector3(x, -8, z), mass, friction, restitution, beginTexture.str(), world));
		iD++;
		cout << beginTexture.str() << endl;
		beginTexture.str("");
		beginTexture.clear();
	}
	for (int z = -38; z < -32; z+=2)
	{
		int x = -20;
		beginTexture << text << iD;
		beginTexture << extension;
		balls.push_back(new PoolBal(iD, 1, btVector3(x, -8, z), mass, friction, restitution, beginTexture.str(), world));
		iD++;
		cout << beginTexture.str() << endl;
		beginTexture.str("");
		beginTexture.clear();
	}
	for (int z = -37; z < -33; z+=2)
	{
		int x = -18;
		beginTexture << text << iD;
		beginTexture << extension;
		balls.push_back(new PoolBal(iD, 1, btVector3(x, -8, z), mass, friction, restitution, beginTexture.str(), world));
		iD++;
		cout << beginTexture.str() << endl;
		beginTexture.str("");
		beginTexture.clear();
	}
	balls.push_back(new PoolBal(15, 1, btVector3(-16, -8, -36), mass, friction, restitution, "data/Pool3D/PoolBalls/15.jpg", world));


	//textures
	BiljardTable = CaveLib::loadModel("data/Pool3D/PoolTable/PoolTable.obj", new ModelLoadOptions(75, true));
	skyboxTexture = CaveLib::loadTexture("data/CubeMaps/Cloudy/total.png");
	balltexture = CaveLib::loadTexture("data/Pool3D/PoolBalls/5.jpg");
	ball = CaveLib::loadModel("sphere.shape");
	groundtexture = CaveLib::loadTexture("data/pool3d/textures/floor3.jpg");

	//Muren
	for (int i = 0; i < muren.size(); i++)
	{
		muren[i]->draw();
	}

	// creation of the floor panels
	float x = -125, z = -125;
	for (int i = 0; i < 2; i++)
	{
		for (int j = 0; j < 2; j++)
		{
			FloorPane floorBox(x, 93, z, 250);
			floorCubes.push_back(floorBox);
			z += 250;
		}
		z = -125;
		x += 250;
	}

	// creation of the skybox
	skybox = new Cube(0, 0, -2, 0, 500, 0, 0, 0, 0);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
}

void MyApplication::createPoolBalls()
{
	for (int i = 1; i < 16; i++)
		ballIds.push_back(new ballId(i, false));
}

void MyApplication::checkButtons()
{
	if (GetAsyncKeyState('B'))
	{
		fall = true;
	}
	if (GetAsyncKeyState('M'))
	{
		if (debug == false)
			debug = true;
		else
			debug = false;
	}
	if (GetAsyncKeyState('T'))
	{
		if (oculus)
			whiteBal->getBulletBody()->applyCentralImpulse(btVector3(0, 0, -5));
		else
			whiteBal->getBulletBody()->applyCentralImpulse(btVector3(0, 0, -10));
		
	}
	if (GetAsyncKeyState('G'))
	{
		if (oculus)
			whiteBal->getBulletBody()->applyCentralImpulse(btVector3(0, 0, 5));
		else
			whiteBal->getBulletBody()->applyCentralImpulse(btVector3(0, 0, 10));
	}
	if (GetAsyncKeyState('F'))
	{
		if (oculus)
			whiteBal->getBulletBody()->applyCentralImpulse(btVector3(-5, 0, 0));
		else
			whiteBal->getBulletBody()->applyCentralImpulse(btVector3(-10, 0, 0));
	}
	if (GetAsyncKeyState('H'))
	{
		if (oculus)
			whiteBal->getBulletBody()->applyCentralImpulse(btVector3(5, 0, 0));
		else
			whiteBal->getBulletBody()->applyCentralImpulse(btVector3(10, 0, 0));
	}
	if (GetAsyncKeyState(VK_SPACE))
	{
		whiteBal->getBulletBody()->applyCentralImpulse(btVector3(0, 10, 0));
	}
	if (GetAsyncKeyState('I'))
	{
		for each (PoolBal* ball in balls)
		{
			cout << "De bal in kwestie is " << ball->getID() << endl;
			cout << "De Z positie van de bal is " << ball->getBulletBody()->getCenterOfMassPosition().getZ() << endl;
			cout << "De X positie van de bal is " << ball->getBulletBody()->getCenterOfMassPosition().getX() << endl;
			cout << "De Y positie van de bal is " << ball->getBulletBody()->getCenterOfMassPosition().getY() << endl;
		}
		cout << "De speler die aan de beurt is is: " << game.getPlayerShooting()->getId() << " met baltype: " << game.getPlayerShooting()->getBallType() 
			<< " met een score van " << game.getPlayerShooting()->getScore() << endl;
	}
}

void setLookDirection(Cue *cueNode, const btVector3& newLook)
{
	cueNode->getBulletBody()->activate();

	btVector3 localLook(0.0f, 1.0f, 0.0f);
	btVector3 rotationAxis(1.0f, 0.0f, 0.0f);

	btTransform transform = cueNode->getBulletBody()->getCenterOfMassTransform();
	btQuaternion rotation = transform.getRotation();
}

void MyApplication::placeCueToWhiteBall()
{
	btVector3 lookDirection = whiteBal->getBulletBody()->getCenterOfMassPosition() - cueStick->getBulletBody()->getCenterOfMassPosition();
	float lookLength = lookDirection.length();
	if (lookLength > FLT_EPSILON)
	{
		lookDirection /= lookLength;
		setLookDirection(cueStick,lookDirection);
	}
}

void MyApplication::checkBallsPocketed()
{
	for (int i = 0; i < balls.size(); i++)
	{
		if (balls.at(i)->getBulletBody()->getCenterOfMassPosition().getY() < -8.5)
		{
			if (game.getBallType(game.getPlayerShooting()) == 0) // 0 = no ball type
			{
				if (balls.at(i)->getID() <= 7)
				{
					game.setBallType(game.getPlayerShooting(), 2); // 2 is solid ball type
					game.setBallType(game.getPlayerNotShooting(), 1); // 1 is striped ball type
					game.setScoreToPlayer(game.getPlayerShooting());
				}
				else if (balls.at(i)->getID() >= 9)
				{
					game.setBallType(game.getPlayerShooting(), 1); // 1 is striped ball type
					game.setBallType(game.getPlayerNotShooting(), 2); // 2 is solid ball type
					game.setScoreToPlayer(game.getPlayerShooting());
				}
			}
			else if (game.getBallType(game.getPlayerShooting()) == 1) // striped balls
			{
				if (balls.at(i)->getID() <= 7)
				{
					game.changePlayerToShoot();
					game.setScoreToPlayer(game.getPlayerShooting());
				}
				else if (balls.at(i)->getID() >= 9)
					game.setScoreToPlayer(game.getPlayerShooting());
			}
			else if (game.getBallType(game.getPlayerShooting()) == 2) // solid balls
			{
				if (balls.at(i)->getID() <= 7)
					game.setScoreToPlayer(game.getPlayerShooting());
				else if (balls.at(i)->getID() >= 9)
				{
					game.changePlayerToShoot();
					game.setScoreToPlayer(game.getPlayerShooting());
				}
			}
			balls.erase(balls.begin() + i);
		}
	}
	if (whiteBal->getBulletBody()->getCenterOfMassPosition().getY() < -8.5)
	{
		world->removeRigidBody(whiteBal->getBulletBody());
		delete(whiteBal);
		whiteBal = new PoolBal(0, 1, btVector3(20, -8, -36), 20, 1.5, 1.0, "data/Pool3D/PoolBalls/BallCue.jpg", world);
		game.changePlayerToShoot();
	}
}
