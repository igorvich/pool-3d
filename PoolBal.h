#pragma once
#include <GL/glew.h>

#include "BulletObject.h"
#include <CaveLib/model.h>
#include <cavelib/texture.h>
#include <CaveLib\Shader.h>
#include <glm/gtc/matrix_transform.hpp>
#include <string>



using namespace std;

class PoolBal
{
	int id;
	BulletObject* bulletObject; 
	cModel* ball;
	cTexture* texture;
	bool textureEmpty = false;
	GLuint textureId;
	ShaderProgram* textureShader;
	glm::vec3 location;
public:
	PoolBal(int id, float radius, btVector3& origin, int massa, float friction
			, float restitution, string texture, btDiscreteDynamicsWorld* world);
	~PoolBal();
	void draw(const glm::mat4 mvp, glm::mat4 modelViewMatrix);

	int getID(){ return  id; }
	btRigidBody* getBulletBody(){ return bulletObject->body; }
	PoolBal* getPoolbal(){ return this; }
	void setLocation(int x, int y , int z);
};

