#include "Game.h"
#include "Player.h"
#include "PoolBal.h"

Game::Game()
{
}

Game::Game(int players)
{
	nrOfPlayers = players;
	if (nrOfPlayers == 1)
	{
		player1 = new Player();
		player1->setId(1);
		player1->setShooting(true);
	}
	else if (nrOfPlayers == 2)
	{
		player1 = new Player();
		player1->setId(1);
		player1->setShooting(true);
		player2 = new Player();
		player2->setId(2);
	}
}

Player* Game::getPlayerShooting()
{
	if (player1->isShooting())
		return player1;
	else if (player2->isShooting())
		return player2;
}

Player* Game::getPlayerNotShooting()
{
	if (player1->isShooting())
		return player2;
	else if (player2->isShooting())
		return player1;
}

void Game::changePlayerToShoot()
{
	if (player1->isShooting())
	{
		player1->setShooting(false);
		player2->setShooting(true);
	}
	else if (player2->isShooting())
	{
		player2->setShooting(false);
		player1->setShooting(true);
	}
}

void Game::setBallType(Player* play, int type)
{
	play->setBallType(type);
}

int Game::getBallType(Player* play)
{
	return play->getBallType();
}

void Game::setScoreToPlayer(Player* play)
{
	play->incrementScore();
}

Game::~Game()
{
}
